package fr.cnam.foad.nfa035.dao;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;

/**
 * Classe de couche DAO
 */
public class BadgeWalletDAO {


    public BadgeWalletDAO(String s) {
        daoPath = Paths.get(s);
    }

    public Path getDaoPath() {
        return daoPath;
    }

    private Path daoPath;


    /**
     * permet d'ajouter un badge (image serialisé) au badgeWallet
     *
     * @param image
     * @throws IOException
     */
    public void addBadge(File image) throws IOException {
        byte[] fileContent = Files.readAllBytes(image.toPath());
        String encodedString = Base64.getEncoder().encodeToString(fileContent);
        Files.writeString(getDaoPath(), encodedString);
    }


    /**
     * permet de recupérer un badge du badgeWallet
     *
     * @param imageStream
     * @throws IOException
     */
    public void getBadge(OutputStream imageStream) throws IOException {
        String content = new String(Files.readAllBytes(getDaoPath()));
        byte[] decodedBytes = Base64.getDecoder().decode(content);
        imageStream.write(decodedBytes);
    }
}