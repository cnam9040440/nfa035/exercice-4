import fr.cnam.foad.nfa035.dao.BadgeWalletDAO;
import fr.cnam.foad.nfa035.fileutils.simpleaccess.ImageSerializer;
import fr.cnam.foad.nfa035.fileutils.simpleaccess.ImageSerializerBase64Impl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.file.Files;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Classe permettant de tester la classe BadgetWalletDAO
 */
public class DAOTest {

    private static final Logger LOG = LogManager.getLogger(DAOTest.class);
    private static final String RESOURCES_PATH = "src/test/resources/";
    private static final File walletDatabase = new File(RESOURCES_PATH+ "wallet.csv");

    @BeforeEach
    public void init() throws IOException {
        if (walletDatabase.exists()){
            walletDatabase.delete();
            walletDatabase.createNewFile();
        }
    }

    @Test
    /**
     * test permettant de tester l'ajout d'un badge au badge wallet
     */
    public void testAddBadge(){
        try {
            File image = new File(RESOURCES_PATH + "petite_image.png");
            BadgeWalletDAO dao = new BadgeWalletDAO(RESOURCES_PATH + "wallet.csv");
            dao.addBadge(image);

            String serializedImage = new String(Files.readAllBytes(walletDatabase.toPath()));
            LOG.info("Le badge-wallet contient à présent cette image sérialisée:\n{}", serializedImage);

            // Utilisation des outils pour comparer avec le résultat attendu
            ImageSerializer serializer = new ImageSerializerBase64Impl();
            String encodedImage = (String) serializer.serialize(image);

            // astuce pour ignorer les différences de formatage entre outils de sérialisation base64:
            serializedImage = serializedImage.replaceAll("\n","").replaceAll("\r","") ;

            assertEquals(serializedImage, encodedImage);

        } catch (Exception e) {
            LOG.error("Test en échec ! ",e);
            fail();
        }
    }


    @Test
    /**
     * test permettant de récupérer un badge du badge wallet
     */
    public void testGetBadge(){
        try {
            BadgeWalletDAO dao = new BadgeWalletDAO(RESOURCES_PATH + "wallet_full.csv");
            File extractedImage = new File(RESOURCES_PATH + "petite_image_extraite.png");

            ByteArrayOutputStream memoryBadgeStream = new ByteArrayOutputStream();
            dao.getBadge(memoryBadgeStream);
            byte[] deserializedImage = memoryBadgeStream.toByteArray();

            // Vérification 1
            byte [] originImage = Files.readAllBytes(new File(RESOURCES_PATH + "petite_image.png").toPath());

            assertArrayEquals(originImage, deserializedImage);
            assertArrayEquals(Arrays.copyOfRange(originImage,0,originImage.length), deserializedImage);

            // Vérification 2
           dao = new BadgeWalletDAO(RESOURCES_PATH + "wallet_full.csv");
            OutputStream fileBadgeStream = new FileOutputStream(extractedImage);
            dao.getBadge(fileBadgeStream);

            deserializedImage = Files.readAllBytes(new File(RESOURCES_PATH + "petite_image_extraite.png").toPath());
            assertArrayEquals(Arrays.copyOfRange(originImage,0,originImage.length), deserializedImage);

        } catch (Exception e) {
            LOG.error("Test en échec ! ",e);
            fail();
        }
    }

}
